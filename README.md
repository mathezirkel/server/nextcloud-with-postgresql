# Nextcloud with PostgreSQL

## Badges 

[![pipeline status](https://gitlab.com/mathezirkel/server-administration/badges/main/pipeline.svg)](https://gitlab.com/mathezirkel/server-administration/-/commits/main)

![License: GPL-3.0](https://img.shields.io/badge/License-GPL--3.0-green)
![AppVersion Nextcloud: 24.0.5-fpm-alpine](https://img.shields.io/badge/AppVersion_Nextcloud-24.0.5--fpm--alpine-informational?style=flat-square)
![AppVersion PostgreSQL: postgresql14](https://img.shields.io/badge/AppVersion_PostgreSQL-postgresql14-informational?style=flat-square)


## Description
The docker image is based on Nextcloud and additionally contains PostgreSQL dependencies.
This is required to use Nextcloud backup app in a containerized environment.


## How to publish to the private registry on Gitlab

Build the image with as follow:
```commandline
$ export TAG=CHANGEME
$ readarray -t build_args < "./docker/build_args.env"
$ for line in "${build_args[@]}"; do export "$line"; done
$ docker build \
    --build-arg NEXTCLOUD_TAG="$NEXTCLOUD_TAG" \
    --build-arg POSTGRES_IMAGE="$POSTGRES_IMAGE" \
    -t registry.gitlab.com/mathezirkel/server/nextcloud-with-postgresql/nextcloud:$TAG \
    ./docker/
```

After logging in to the registry at https://gitlab.com/mathezirkel/server/nextcloud-with-postgresql/container_registry/
via
```commandline
$ docker login registry.gitlab.com
```
you can push the image to Gitlab by executing e.g.
```commandline
$ docker push registry.gitlab.com/mathezirkel/server/nextcloud-with-postgresql/nextcloud:$TAG
```

## License
This project is licenced unter GPL-3.0, see [LICENSE](./LICENSE).
